#!/usr/bin/env python3

import argparse
import fileinput

import prettyxml

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "files",
        metavar="FILE",
        nargs="*",
        help="files to read, if empty, stdin is used",
    )
    args = parser.parse_args()

    with fileinput.input(files=args.files) as f:
        print(prettyxml.process("".join(f)))
